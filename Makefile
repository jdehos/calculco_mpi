all:
	mpic++ -O2 -std=c++11 -Wall -Wextra -Wno-unknown-pragmas -o laplacien_mpi_1.out laplacien_mpi_1.cpp 
	mpic++ -O2 -std=c++11 -Wall -Wextra -Wno-unknown-pragmas -o laplacien_mpi_2.out laplacien_mpi_2.cpp 
	g++ -O2 -std=c++11 -Wall -Wextra -fopenmp -o laplacien_omp.out laplacien_omp.cpp 

clean:
	rm -f laplacien_*.out 
