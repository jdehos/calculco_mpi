# test de MPI sur calculco

## description
 
- algo distribué qui découpe une image, l'envoie aux processus esclaves (calcul du laplacien) et fusionne les résultats
- code C++ avec la bibliothèque mpi (C)
- exécuté avec l'environnement openmpi

## compilation 

```
make
```

## exécution sur une grille OAR 

```
oarsub -S ./run.sh
```

## exécution en local

```
./plot_local.sh
```

- mpi1 : code MPI "proche du code séquentiel" 
- mpi2 : code MPI "avec un gros if master/slave"
- omp : code OpenMP (qui considère que les données sont accessibles localement)

![](plot_local.png)

*CPU 6 coeurs hyper-threading (12 coeurs virtuels)*

\

## références

- [http://oar.imag.fr/docs/latest/user/usecases.html](http://oar.imag.fr/docs/latest/user/usecases.html)
- [http://calculs.unice.fr/fr/assistance/gestion-soumission-travaux/exemples-scripts-oar](http://calculs.unice.fr/fr/assistance/gestion-soumission-travaux/exemples-scripts-oar)


