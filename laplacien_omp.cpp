// g++ -std=c++11 -Wall -Wextra -o laplacien_omp.out laplacien_omp.cpp 
// OMP_NUM_THREADS=4 ./laplacien_omp.out canyon.pgm canyon_omp.pgm
// mix master code and slave code 

#include "image.hpp"
#include <algorithm>
#include <omp.h>

int main(int argc, char ** argv)
{
    if (argc != 5)
    {
        std::cout << "usage: " << argv[0] 
            << " <input> <output> <scaling> <nb fakes>\n";
        exit(-1);
    }
    const char * INPUT = argv[1];
    const char * OUTPUT = argv[2];
    const float SCALING = atof(argv[3]);
    const int NB_FAKES = atoi(argv[4]);

    double t0 = omp_get_wtime();

    // read image 
    image_t data0;
    int width, height;
    std::string readError = readPgm(INPUT, width, height, data0);
    if (readError != "")
    {
        std::cout << readError << std::endl;
        exit(-1);
    }

    // compute whole image
    image_t data2 = computeLaplacianOmp(data0, width, height, SCALING);
    for (int k=0; k<NB_FAKES; ++k)
        data2 = computeLaplacianOmp(data0, width, height, SCALING);

    // write output image
    writePgm(OUTPUT, width, height, data2);

    double t1 = omp_get_wtime();
    std::cout << t1 - t0;

    return 0;
}

