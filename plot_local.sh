#!/bin/sh

NB_THREADS_MAX=15
SCALING=100
NB_FAKES=10000

PGM="backloop.pgm"
CSV="times.csv"
SVG="times.svg"

rm -f ${CSV}
TMP=`mktemp`
for n in `seq ${NB_THREADS_MAX}` ; do
    echo "${n} thread(s)"
    echo -e "${n}; \c" >> ${CSV}
    mpirun -n ${n} ./laplacien_mpi_1.out ${PGM} ${TMP} ${SCALING} ${NB_FAKES} | awk -v ORS="; " '{print $1}' >> ${CSV}
    mpirun -n ${n} ./laplacien_mpi_2.out ${PGM} ${TMP} ${SCALING} ${NB_FAKES} | awk -v ORS="; " '{print $1}' >> ${CSV}
    OMP_NUM_THREADS=${n} ./laplacien_omp.out ${PGM} ${TMP} ${SCALING} ${NB_FAKES} | awk '{print $1}' >> ${CSV}
done
rm ${TMP}
echo "output: ${CSV}"

gnuplot -e "set out '${SVG}'; \
    set terminal svg size 640,480; \
    set style data linespoints; \
    set grid xtics ytics; \
    plot '${CSV}' using 1:2 title 'mpi1', \
    '${CSV}' using 1:3 title 'mpi2', \
    '${CSV}' using 1:4 title 'omp';
"
echo "output: ${SVG}"

