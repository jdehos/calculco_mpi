#!/bin/sh

#OAR -l /core=50,walltime=1
#OAR -t besteffort

ulimit -s unlimited
NSLOTS=$(cat $OAR_NODEFILE | wc -l)
PREF=$(dirname `which mpirun` | awk -F'/[^/]*$' '{print $1}')
mpirun --prefix $PREF -np $NSLOTS -machinefile $OAR_NODEFILE -mca orte_rsh_agent "oarsh" ./laplacien_mpi_1.out canyon.pgm canyon_mpi_1.pgm 100 10
exit $?

